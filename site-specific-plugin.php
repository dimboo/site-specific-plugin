<?php
/*
Plugin Name: Site Specific Plugin
Description: Site specific plugin for dimboo sites
Version: 0.3.0
Author: Dimitri Boone
Author URI: http://dimboo.net/
License: GPLv2
*/


// Auto-update plugins & themes
// To auto-update only selected plugins install https://wordpress.org/support/view/plugin-reviews/automatic-plugin-updates
add_filter( 'auto_update_plugin', '__return_true' );
add_filter( 'auto_update_theme', '__return_true' );

// Disable Theme & Plugin Editor
define( 'DISALLOW_FILE_EDIT', true );

// Disable Theme Editor
function remove_editor_menu() {
    remove_action('admin_menu', '_add_themes_utility_last', 101);
}
add_action('_admin_menu', 'remove_editor_menu', 1);

// Redirect to random post
// uncomment add_action lines below to activate

//add_action('init','random_add_rewrite');
function random_add_rewrite() {
       global $wp;
       $wp->add_query_var('random');
       add_rewrite_rule('random/?$', 'index.php?random=1', 'top');
}

//add_action('template_redirect','random_template');
function random_template() {
       if (get_query_var('random') == 1) {
               $posts = get_posts('post_type=post&orderby=rand&numberposts=1');
               foreach($posts as $post) {
                       $link = get_permalink($post);
               }
               wp_redirect($link,307);
               exit;
       }
}

// Disable Visual Editor
function dimboo_disable_visual_editor(){
    # add logic here if you want to permit it selectively
    return false;
}
add_filter('user_can_richedit' , 'dimboo_disable_visual_editor', 50);









?>